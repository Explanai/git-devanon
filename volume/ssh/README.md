KEEP THIS FOLDER!
---

This is where you will keep your RSA keys and config file for SSH access to GitHub. When you build the container with docker-compose build, there is a step where all content inside this directory will be copied to _/root/.ssh_ inside the container. The _/root/.ssh_ folder is the default place for storing ssh keys and the ssh config file.