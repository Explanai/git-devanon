#!/bin/bash
echo "Starting tor service"
service tor start

echo "Starting privoxy service"
service privoxy start

echo "Exporting proxy port"
export http_proxy="http://127.0.0.1:8118"
export https_proxy="https://127.0.0.1:8118"

#echo "This is your new IP | using 'curl http://icanhazip.com/"
#curl 'http://icanhazip.com/'

#echo "This is your new IP | using 'curl -x 127.0.0.1:8118 http://icanhazip.com/'"
#curl -x 127.0.0.1:8118 'http://icanhazip.com/'

echo "Container will hang now. Open another terminal session and run 'docker exec -it gitdevanon bash' to access container."
tail -f /dev/null