FROM debian:buster-slim

RUN mkdir /workdir
RUN mkdir /.ssh
WORKDIR /workdir
COPY . /workdir

COPY ./volume/ssh/* /.ssh

RUN chmod +x requirements.sh
RUN chmod +x entrypoint.sh

RUN sh requirements.sh

EXPOSE 8118 9050 9051

ENTRYPOINT ["./entrypoint.sh"]