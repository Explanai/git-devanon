#!/bin/bash
echo "Starting system update and upgrade"
apt-get update && apt-get upgrade -y

apt install curl -y

echo "Installing tor, privoxy and git"
apt-get install tor privoxy git -y

echo "Configuring privoxy socket"
echo "forward-socks5t / 127.0.0.1:9050 ." >> /etc/privoxy/config

echo "Disabling listening on IPv6 because privoxy can't use it"
sed -i "s/.*\[::1\]:8118/# &/" /etc/privoxy/config

