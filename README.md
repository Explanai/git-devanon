# GitDevAnon

---
## Introduction

_GitDevAnon_ is Debian + Tor + Privoxy + Git + Docker working altogether to help you anonymize the git commits of your development process.

It works setting up a Tor Proxy and a volume mapped with your git project(s). All you have to do is run the container, set up your credentials and make sure to **ALWAYS** communicate with Git via _GitDevAnon_ container.

---
## Prerequisites

You will need Docker installed in your machine. Also, you may need Git installed to download this project. That's all! :)

---

## First steps

After setting up Docker and download this project all you have to do if configure the volume you will use to keep your project and your git credentials. 
You can read more about volumes in Docker here: https://docs.docker.com/storage/volumes/

To configure your volume you need to open the docker-compose.yml file inside the root directory of this project and change the `<YOUR-DIRECTORY-PATH-HERE>` flag to whatever path in your system. Just remember to **ONLY** use this directory to the files you want to handle with _GitDevAnon_ service running.

Example:

``
    volumes:
      - /home/my-user/my-anon-project/:/tmp/
``

You can see that we are creating a volume in the _/tmp_ folder inside the container, so everything that you add inside your my-anon-project folder will be inside the container's /tmp folder, and vice-versa. You can change tmp to whatever you want, like the root directory(_/_):

``
    volumes:
      - /home/my-user/my-anon-project/:/
``

After setting up your volume in the _docker-compose.yml_ file, you can now build your container

``
docker-compose build
``

This command will only work if you're in the root directory of the _GitDevAnon_ project.

Next you want to start your container. 

``
docker-compose up
``

---
## Running GitDevAnon

Onde the container is up you will see the message below:

``
Container will hang now. Open another terminal session and run 'docker exec -it gitdevanon bash' to access container.
``

After the message you will notice that the terminal will hang. You will now need to open another terminal to get into the container.
You will need to execute:

``
docker exec -it gitdevanon bash
``

You will notice that you're now inside the container. To check if your IP have changed, you can run curl with some ipcheck site. We will run curl with _icanhazip.com_:

``
curl -x 127.0.0.1:8118 'http://icanhazip.com/'
``

The command above will make request to icanhazip by the proxy configured in port 8118. You will get a new IP that belongs to the TOR network now. You can compare it with the IP of using one of the methods: 


1) Run the curl command above without the _-x_ flag:
``
curl 'http://icanhazip.com/'
``
 

2) Open your favourite browser and search for IP Check in the search engine of your choice then compare it with the returned IP from curl.

If the IPs **are the same**, it means that **YOU ARE NOT** running the container through Tor Proxy. Don't continue to the next steps in this case.

If you checked the IP and they **are not** the same, it means that your tor proxy is working. You can now run the GIT commands you may need in order to proceeed with your development.

---

##~~Setting up Proxy in your navigator~~

~~You may want to access some sites with your own navigator and take some advantage from all the previous configuration you've done. For this, **don't forget to keep you _GitDevAnon_ container running!**~~

~~You can find how to configure a proxy server in some of the most famous browsers [here](https://www.digitalcitizen.life/how-set-proxy-server-all-major-internet-browsers-windows/).~~

~~The informations you will need to the configurations are:~~

~~HTTP Proxy: 127.0.0.1 | Port: 8118~~

~~Socks Host (v5): 127.0.0.1 | Port: 9050~~

~~After you finish it, go to https://check.torproject.org/ and check the result. If it's all ok, the site will inform that the browser is configured to use Tor and give you your new IP.~~

###Don't follow the steps above. Instead, download Tor Browser for this kind of navigation. Use it to create another e-mail and your Github account.
###You can download Tor Browser [here](https://www.torproject.org/download/).

---
##TIPS

- Create another email for anonymous registration.


- Create another user in GitHub and save **everyting** you may need (like your private key, for example) inside your volume so it will be persisted in your local machine.


- Run the "_docker exec -it gitdevanon bash_" inside the IDE of your choice and use ONLY this terminal to interact with GIT.
 
---
## Setting up the SSH Keys 

If you already have your SSH keys inside the .ssh file, you can pass this step. Otherwise, you will need to create a new ssh key.

```
ssh-keygen -t rsa -b 4096 -C "YOUR-PRIVATE_EMAIL@HERE.COM"
```
Note that the -C "YOUR-PRIVATE-EMAIL@HERE.COM" is optional. You can go just delete this part if you want.

You can create a name for the keys, but you will have to change the default name in "_.ssh/config_" later. If you keep the original names (id_rsa and id_rsa.pub) you won't need to change it.

Save it in the volume you've mapped in the configurations of _GitDevAnon_ _docker-compose.yml_. Then, access https://github.com/settings/keys and add your new SSH key (**it's the .pub file!**).

Now you probably have saved your key in the standard folder /root/.sh but that's place is not our volume. So you can copy it to our volume to keep it persisted even after the container is not up.
To accomplish that you may use cp.

```
cp /root/.ssh/* /volume/.ssh/
```

Now that you have your keys, you need to copy the key ending with .pub and add it [here](https://github.com/settings/keys).

Don't forget that, once you copied your keys to /volume/.ssh, the next time you execute the container, this folder will be copied to /root/.ssh inside the container. Read the README.md file inside the .ssh folder of this project to know more.

Now you can go to volume/projects and start your configuration using:

    git init
    git config --add user.name <your git username>
    git config --add user.email <your anonymous email>

You can now get the project in your git using:

    git remote add origin ssh://git@github-tor/<user>/<project>.git
    git push origin master
